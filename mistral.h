#ifndef MISTRAL_H
#define MISTRAL_H

#define PHP_MISTRAL_VERSION "0.1.0"

extern zend_module_entry mistral_module_entry;
#define phpext_mistral_ptr &mistral_module_entry

#ifdef ZTS
#include "TSRM.h"
#endif

PHP_MSHUTDOWN_FUNCTION(mistral);
PHP_MINFO_FUNCTION(mistral);

PHP_FUNCTION(mistral_init);
PHP_FUNCTION(mistral_register_callback);
PHP_FUNCTION(mistral_start);

#define php_strcasecmp(string, php_string, php_len) ((strlen(string)) == php_len && strncasecmp(string, php_string, php_len) == 0)
#define php_strcmp(string, php_string, php_len) ((strlen(string)) == php_len && strncmp(string, php_string, php_len) == 0)

#endif	/* MISTRAL_H */
