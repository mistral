#ifndef _http11_common_h
#define _http11_common_h

#include <sys/types.h>

typedef void (*field_cb)(void *data, const char *field, size_t flen, const char *value, size_t vlen);
typedef void (*element_cb)(void *data, int type, const char *at, size_t length);

#endif
