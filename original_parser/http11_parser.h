
#ifndef http11_parser_h
#define http11_parser_h

#include "http11_common.h"

enum { CONTENT_LENGTH
     , CONTENT_TYPE
     , FRAGMENT
     , HTTP_VERSION
     , QUERY_STRING
     , REQUEST_PATH
     , REQUEST_METHOD
     , REQUEST_URI
     };

typedef struct http_parser { 
  int cs;
  size_t body_start;
  int content_len;
  size_t nread;
  size_t mark;
  size_t field_start;
  size_t field_len;
  size_t query_start;

  void *data;

  field_cb http_field;
  element_cb on_element;  
} http_parser;

int http_parser_init(http_parser *parser);
int http_parser_finish(http_parser *parser);
size_t http_parser_execute(http_parser *parser, const char *data, size_t len, size_t off);
int http_parser_has_error(http_parser *parser);
int http_parser_is_finished(http_parser *parser);

#define http_parser_nread(parser) (parser)->nread 

#endif
