<?php
function my_cool_callback($requests) {
    $requests['HTTP_CONNECTION'] = isset($requests['HTTP_CONNECTION']) ? $requests['HTTP_CONNECTION'] : 'close';

    $output = "Hello World! PID: " . getMyPid();
    $answer = array('status_code' => '200 OK',
                'connection' => $requests['HTTP_CONNECTION'],
                'content-type' => 'text/html',
                'content-length' => strlen($output),
                'body' => $output);
    return $answer;
}

$cntChildren = 0;
$maxChildren = 4;
$startport = 8080;
while( $cntChildren < $maxChildren ) {
    $cntChildren++;
    $startport++;

    $pid = pcntl_fork();
    if ($pid == -1) {
        echo "Warning: Cannot spawn child process!!!\n";
    } elseif($pid) {
        // Parent process
    } else {
        echo '[' . getMyPid() . '] Child process listening on port ' . $startport . "\n";
        mistral_init('0.0.0.0', $startport, 5);
        mistral_register_callback('my_cool_callback');
        mistral_start();
    }
}

while(true) {
    sleep(1);
}

