<?php
function my_cool_callback($requests) {
    /* Takes as argument: status_code, body, and all other headers. The above, string based still works too. */
    return array('status_code' => '200 OK', 'connection' => 'close', 'content-type' => 'text/plain', 'body' => 'Hello World!');
}

$port = 8081;

# Bind Webserver on every ip with the port, timeout > 0 == keep-alive
mistral_init('0.0.0.0', $port, 0);

# Setup callback function which gets called after each client request
mistral_register_callback("my_cool_callback");

echo "Mistral Listening on port $port\n";

# Start serving
mistral_start();

