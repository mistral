<?php
function my_cool_callback($requests) {
    $requests['HTTP_CONNECTION'] = isset($requests['HTTP_CONNECTION']) ? $requests['HTTP_CONNECTION'] : 'close';

    $output = print_r($requests, true);

    $answer = array('status_code' => '200 OK',
                'connection' => $requests['HTTP_CONNECTION'],
                'content-type' => 'text/plain',
                'content-length' => strlen($output),
                'body' => $output);
    return $answer;
}

$port = 8081;

# Bind Webserver on all ips with port 8081, timeout > 0 == keep-alive
mistral_init('0.0.0.0', $port, 2);

# Setup callback function which gets called after each client request
mistral_register_callback("my_cool_callback");

echo "Mistral Listening on port $port\n";

# Start serving
mistral_start();
