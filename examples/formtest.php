<?php
function my_cool_callback($requests) {
    $requests['HTTP_CONNECTION'] = isset($requests['HTTP_CONNECTION']) ? $requests['HTTP_CONNECTION'] : 'close';

    $output = '<form method="post" enctype="multipart/form-data"><input type="text" name="test_txt_fld" /><input type="hidden" name="test_hdn_fld" value="foobar" /><input type="file" name="testfile" /><input type="submit" /></form>';
    $output .= nl2br(print_r($requests, true));

    $answer = array('status_code' => '200 OK',
                'connection' => $requests['HTTP_CONNECTION'],
                'content-type' => 'text/html',
                'content-length' => strlen($output),
                'body' => $output);
    return $answer;
}

$port = 8081;

# Bind Webserver on all ips with the port, timeout > 0 == keep-alive
mistral_init('0.0.0.0', $port, 2);

# Setup callback function which gets called after each client request
mistral_register_callback("my_cool_callback");

echo "Mistral Listening on port $port\n";

# Start serving
mistral_start();
