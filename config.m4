dnl $Id$
dnl config.m4 for extension mistral
dnl Comments in this file start with the string 'dnl'.

PHP_ARG_WITH(libev, for libev support,
dnl Make sure that the comment is aligned:
    [  --with-libev[=DIR]   Include libev support])

dnl if test "$PHP_LIBEV" != "no"; then
	AC_MSG_CHECKING(for libev install directory)
	for i in $PHP_LIBEV /usr /usr/local; do
		if test -r $i/include/libev/ev.h; then
			LIBEV_INC_DIR=$i/include/libev
		elif test -r $i/include/ev.h; then
			LIBEV_INC_DIR=$i/include
		fi

		if test -r $i/lib64/libev.so; then
			LIBEV_DIR=$i/lib64
		elif test -r $i/lib/libev.so; then
			LIBEV_DIR=$i/lib
		fi
	done
	
	AC_MSG_RESULT($LIBEV_DIR)
	PHP_ADD_INCLUDE($LIBEV_INC_DIR)

	PHP_SUBST(MISTRAL_SHARED_LIBADD)
	PHP_ADD_LIBRARY_WITH_PATH(ev, $LIBEV_DIR, MISTRAL_SHARED_LIBADD)

	AC_DEFINE(HAVE_LIBEV,1,[Whether you have libev])
	
	PHP_NEW_EXTENSION(mistral, mistral.c http11_parser.c, $ext_shared)
dnl fi