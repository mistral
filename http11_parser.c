
#line 1 "http11_parser.rl"
/**
 *
 * Copyright (c) 2010, Zed A. Shaw and Mongrel2 Project Contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 * 
 *     * Neither the name of the Mongrel2 Project, Zed A. Shaw, nor the names
 *       of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written
 *       permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "http11_parser.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define LEN(AT, FPC) (FPC - buffer - parser->AT)
#define MARK(M,FPC) (parser->M = (FPC) - buffer)
#define PTR_TO(F) (buffer + parser->F)

/** Machine **/


#line 161 "http11_parser.rl"


/** Data **/

#line 57 "http11_parser.c"
static const char _http_parser_actions[] = {
	0, 1, 0, 1, 1, 1, 2, 1, 
	3, 1, 4, 1, 5, 1, 6, 1, 
	7, 1, 8, 1, 9, 1, 10, 1, 
	12, 2, 0, 5, 2, 3, 4, 2, 
	7, 9, 2, 11, 9, 3, 10, 11, 
	9
};

static const short _http_parser_key_offsets[] = {
	0, 0, 8, 17, 20, 22, 23, 24, 
	25, 26, 27, 28, 29, 31, 33, 50, 
	51, 67, 70, 72, 73, 82, 91, 97, 
	103, 114, 120, 126, 136, 142, 148, 157, 
	166, 172, 178, 179, 180, 181, 182, 191, 
	197, 203, 212, 221, 230, 239, 248, 257, 
	266, 275, 284, 293, 302, 311, 320, 329, 
	338, 347, 356, 365, 366
};

static const char _http_parser_trans_keys[] = {
	36, 95, 45, 46, 48, 57, 65, 90, 
	32, 36, 95, 45, 46, 48, 57, 65, 
	90, 42, 47, 104, 32, 35, 72, 84, 
	84, 80, 47, 49, 46, 48, 49, 10, 
	13, 10, 13, 33, 124, 126, 35, 39, 
	42, 43, 45, 46, 48, 57, 65, 90, 
	94, 122, 10, 33, 58, 124, 126, 35, 
	39, 42, 43, 45, 46, 48, 57, 65, 
	90, 94, 122, 10, 13, 32, 10, 13, 
	10, 32, 37, 60, 62, 127, 0, 31, 
	34, 35, 32, 37, 60, 62, 127, 0, 
	31, 34, 35, 48, 57, 65, 70, 97, 
	102, 48, 57, 65, 70, 97, 102, 32, 
	34, 35, 37, 59, 60, 62, 63, 127, 
	0, 31, 48, 57, 65, 70, 97, 102, 
	48, 57, 65, 70, 97, 102, 32, 34, 
	35, 37, 60, 62, 63, 127, 0, 31, 
	48, 57, 65, 70, 97, 102, 48, 57, 
	65, 70, 97, 102, 32, 34, 35, 37, 
	60, 62, 127, 0, 31, 32, 34, 35, 
	37, 60, 62, 127, 0, 31, 48, 57, 
	65, 70, 97, 102, 48, 57, 65, 70, 
	97, 102, 116, 116, 112, 58, 32, 34, 
	35, 37, 60, 62, 127, 0, 31, 48, 
	57, 65, 70, 97, 102, 48, 57, 65, 
	70, 97, 102, 32, 36, 95, 45, 46, 
	48, 57, 65, 90, 32, 36, 95, 45, 
	46, 48, 57, 65, 90, 32, 36, 95, 
	45, 46, 48, 57, 65, 90, 32, 36, 
	95, 45, 46, 48, 57, 65, 90, 32, 
	36, 95, 45, 46, 48, 57, 65, 90, 
	32, 36, 95, 45, 46, 48, 57, 65, 
	90, 32, 36, 95, 45, 46, 48, 57, 
	65, 90, 32, 36, 95, 45, 46, 48, 
	57, 65, 90, 32, 36, 95, 45, 46, 
	48, 57, 65, 90, 32, 36, 95, 45, 
	46, 48, 57, 65, 90, 32, 36, 95, 
	45, 46, 48, 57, 65, 90, 32, 36, 
	95, 45, 46, 48, 57, 65, 90, 32, 
	36, 95, 45, 46, 48, 57, 65, 90, 
	32, 36, 95, 45, 46, 48, 57, 65, 
	90, 32, 36, 95, 45, 46, 48, 57, 
	65, 90, 32, 36, 95, 45, 46, 48, 
	57, 65, 90, 32, 36, 95, 45, 46, 
	48, 57, 65, 90, 32, 36, 95, 45, 
	46, 48, 57, 65, 90, 32, 0
};

static const char _http_parser_single_lengths[] = {
	0, 2, 3, 3, 2, 1, 1, 1, 
	1, 1, 1, 1, 0, 2, 5, 1, 
	4, 3, 2, 1, 5, 5, 0, 0, 
	9, 0, 0, 8, 0, 0, 7, 7, 
	0, 0, 1, 1, 1, 1, 7, 0, 
	0, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 1, 0
};

static const char _http_parser_range_lengths[] = {
	0, 3, 3, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 1, 0, 6, 0, 
	6, 0, 0, 0, 2, 2, 3, 3, 
	1, 3, 3, 1, 3, 3, 1, 1, 
	3, 3, 0, 0, 0, 0, 1, 3, 
	3, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 3, 3, 3, 3, 3, 
	3, 3, 3, 0, 0
};

static const short _http_parser_index_offsets[] = {
	0, 0, 6, 13, 17, 20, 22, 24, 
	26, 28, 30, 32, 34, 36, 39, 51, 
	53, 64, 68, 71, 73, 81, 89, 93, 
	97, 108, 112, 116, 126, 130, 134, 143, 
	152, 156, 160, 162, 164, 166, 168, 177, 
	181, 185, 192, 199, 206, 213, 220, 227, 
	234, 241, 248, 255, 262, 269, 276, 283, 
	290, 297, 304, 311, 313
};

static const char _http_parser_indicies[] = {
	0, 0, 0, 0, 0, 1, 2, 3, 
	3, 3, 3, 3, 1, 4, 5, 6, 
	1, 7, 8, 1, 9, 1, 10, 1, 
	11, 1, 12, 1, 13, 1, 14, 1, 
	15, 1, 16, 1, 17, 18, 1, 19, 
	20, 21, 21, 21, 21, 21, 21, 21, 
	21, 21, 1, 19, 1, 22, 23, 22, 
	22, 22, 22, 22, 22, 22, 22, 1, 
	25, 26, 27, 24, 29, 30, 28, 31, 
	1, 33, 34, 1, 1, 1, 1, 1, 
	32, 36, 37, 1, 1, 1, 1, 1, 
	35, 38, 38, 38, 1, 35, 35, 35, 
	1, 40, 1, 41, 42, 43, 1, 1, 
	44, 1, 1, 39, 45, 45, 45, 1, 
	39, 39, 39, 1, 7, 1, 8, 47, 
	1, 1, 48, 1, 1, 46, 49, 49, 
	49, 1, 46, 46, 46, 1, 51, 1, 
	52, 53, 1, 1, 1, 1, 50, 55, 
	1, 56, 57, 1, 1, 1, 1, 54, 
	58, 58, 58, 1, 54, 54, 54, 1, 
	59, 1, 60, 1, 61, 1, 62, 1, 
	7, 1, 8, 63, 1, 1, 1, 1, 
	62, 64, 64, 64, 1, 62, 62, 62, 
	1, 2, 65, 65, 65, 65, 65, 1, 
	2, 66, 66, 66, 66, 66, 1, 2, 
	67, 67, 67, 67, 67, 1, 2, 68, 
	68, 68, 68, 68, 1, 2, 69, 69, 
	69, 69, 69, 1, 2, 70, 70, 70, 
	70, 70, 1, 2, 71, 71, 71, 71, 
	71, 1, 2, 72, 72, 72, 72, 72, 
	1, 2, 73, 73, 73, 73, 73, 1, 
	2, 74, 74, 74, 74, 74, 1, 2, 
	75, 75, 75, 75, 75, 1, 2, 76, 
	76, 76, 76, 76, 1, 2, 77, 77, 
	77, 77, 77, 1, 2, 78, 78, 78, 
	78, 78, 1, 2, 79, 79, 79, 79, 
	79, 1, 2, 80, 80, 80, 80, 80, 
	1, 2, 81, 81, 81, 81, 81, 1, 
	2, 82, 82, 82, 82, 82, 1, 2, 
	1, 1, 0
};

static const char _http_parser_trans_targs[] = {
	2, 0, 3, 41, 4, 24, 34, 5, 
	20, 6, 7, 8, 9, 10, 11, 12, 
	13, 14, 19, 60, 15, 16, 16, 17, 
	18, 14, 19, 17, 18, 14, 19, 14, 
	21, 5, 22, 21, 5, 22, 23, 24, 
	5, 20, 25, 27, 30, 26, 27, 28, 
	30, 29, 31, 5, 20, 32, 31, 5, 
	20, 32, 33, 35, 36, 37, 38, 39, 
	40, 42, 43, 44, 45, 46, 47, 48, 
	49, 50, 51, 52, 53, 54, 55, 56, 
	57, 58, 59
};

static const char _http_parser_trans_actions[] = {
	1, 0, 17, 0, 1, 1, 1, 19, 
	19, 1, 0, 0, 0, 0, 0, 0, 
	0, 13, 13, 23, 0, 3, 0, 5, 
	7, 28, 28, 7, 0, 9, 9, 0, 
	1, 25, 1, 0, 11, 0, 0, 0, 
	31, 31, 0, 15, 15, 0, 0, 0, 
	0, 0, 21, 37, 37, 21, 0, 34, 
	34, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0
};

static const int http_parser_start = 1;
static const int http_parser_first_final = 60;
static const int http_parser_error = 0;

static const int http_parser_en_main = 1;


#line 165 "http11_parser.rl"

static int apply_element(http_parser *parser, int type, const char *begin, const char *end, int max_length)
{
  int len = (int)(end-begin);
  if(len > max_length) {
    return(0);
  }
  if(parser->on_element)
    parser->on_element(parser->data, type, begin, len);
  return(1);
}



int http_parser_init(http_parser *parser) {
  int cs = 0;
  
#line 256 "http11_parser.c"
	{
	cs = http_parser_start;
	}

#line 182 "http11_parser.rl"
  parser->cs = cs;
  parser->body_start = 0;
  parser->content_len = 0;
  parser->mark = 0;
  parser->nread = 0;
  parser->field_len = 0;
  parser->field_start = 0;

  return(1);
}

/** exec **/
size_t http_parser_execute(http_parser *parser, const char *buffer, size_t len, size_t off)  
{
  if(len == 0) return 0;

  const char *p, *pe;
  int cs = parser->cs;

  assert(off <= len && "offset past end of buffer");

  p = buffer+off;
  pe = buffer+len;

  assert(pe - p == (int)len - (int)off && "pointers aren't same distance");

  
#line 289 "http11_parser.c"
	{
	int _klen;
	unsigned int _trans;
	const char *_acts;
	unsigned int _nacts;
	const char *_keys;

	if ( p == pe )
		goto _test_eof;
	if ( cs == 0 )
		goto _out;
_resume:
	_keys = _http_parser_trans_keys + _http_parser_key_offsets[cs];
	_trans = _http_parser_index_offsets[cs];

	_klen = _http_parser_single_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + _klen - 1;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + ((_upper-_lower) >> 1);
			if ( (*p) < *_mid )
				_upper = _mid - 1;
			else if ( (*p) > *_mid )
				_lower = _mid + 1;
			else {
				_trans += (_mid - _keys);
				goto _match;
			}
		}
		_keys += _klen;
		_trans += _klen;
	}

	_klen = _http_parser_range_lengths[cs];
	if ( _klen > 0 ) {
		const char *_lower = _keys;
		const char *_mid;
		const char *_upper = _keys + (_klen<<1) - 2;
		while (1) {
			if ( _upper < _lower )
				break;

			_mid = _lower + (((_upper-_lower) >> 1) & ~1);
			if ( (*p) < _mid[0] )
				_upper = _mid - 2;
			else if ( (*p) > _mid[1] )
				_lower = _mid + 2;
			else {
				_trans += ((_mid - _keys)>>1);
				goto _match;
			}
		}
		_trans += _klen;
	}

_match:
	_trans = _http_parser_indicies[_trans];
	cs = _http_parser_trans_targs[_trans];

	if ( _http_parser_trans_actions[_trans] == 0 )
		goto _again;

	_acts = _http_parser_actions + _http_parser_trans_actions[_trans];
	_nacts = (unsigned int) *_acts++;
	while ( _nacts-- > 0 )
	{
		switch ( *_acts++ )
		{
	case 0:
#line 52 "http11_parser.rl"
	{MARK(mark, p); }
	break;
	case 1:
#line 55 "http11_parser.rl"
	{ MARK(field_start, p); }
	break;
	case 2:
#line 56 "http11_parser.rl"
	{ 
    parser->field_len = LEN(field_start, p);
  }
	break;
	case 3:
#line 60 "http11_parser.rl"
	{ MARK(mark, p); }
	break;
	case 4:
#line 62 "http11_parser.rl"
	{
    if(parser->http_field != NULL) {
      parser->http_field(parser->data, PTR_TO(field_start), parser->field_len, PTR_TO(mark), LEN(mark, p));
    }
  }
	break;
	case 5:
#line 73 "http11_parser.rl"
	{
    if(!apply_element(parser, FRAGMENT, PTR_TO(mark), p, 10*1024))
      {p++; goto _out; }
  }
	break;
	case 6:
#line 78 "http11_parser.rl"
	{
    if(!apply_element(parser, HTTP_VERSION, PTR_TO(mark), p, 10))
      {p++; goto _out; }
  }
	break;
	case 7:
#line 83 "http11_parser.rl"
	{
    if(!apply_element(parser, REQUEST_PATH, PTR_TO(mark), p, 1024))
      {p++; goto _out; }
  }
	break;
	case 8:
#line 88 "http11_parser.rl"
	{
    if(!apply_element(parser, REQUEST_METHOD, PTR_TO(mark), p, 1024))
      {p++; goto _out; }
  }
	break;
	case 9:
#line 93 "http11_parser.rl"
	{
    if(!apply_element(parser, REQUEST_URI, PTR_TO(mark), p, 12*1024))
      {p++; goto _out; }
  }
	break;
	case 10:
#line 98 "http11_parser.rl"
	{MARK(query_start, p); }
	break;
	case 11:
#line 100 "http11_parser.rl"
	{
    if(!apply_element(parser, QUERY_STRING, PTR_TO(query_start), p, 10*1024))
      {p++; goto _out; }
  }
	break;
	case 12:
#line 105 "http11_parser.rl"
	{
    parser->body_start = p - buffer + 1;    
    if(parser->header_done != NULL)
      parser->header_done(parser->data, REQUEST_BODY, p + 1, pe - p - 1);
    {p++; goto _out; }
  }
	break;
#line 444 "http11_parser.c"
		}
	}

_again:
	if ( cs == 0 )
		goto _out;
	if ( ++p != pe )
		goto _resume;
	_test_eof: {}
	_out: {}
	}

#line 209 "http11_parser.rl"

  assert(p <= pe && "Buffer overflow after parsing.");

  if (!http_parser_has_error(parser)) {
      parser->cs = cs;
  }

  parser->nread += p - (buffer + off);

  assert(parser->nread <= len && "nread longer than length");
  assert(parser->body_start <= len && "body starts after buffer end");
  assert(parser->mark < len && "mark is after buffer end");
  assert(parser->field_len <= len && "field has length longer than whole buffer");
  assert(parser->field_start < len && "field starts after buffer end");

  return(parser->nread);
}

int http_parser_finish(http_parser *parser)
{
  if (http_parser_has_error(parser) ) {
    return -1;
  } else if (http_parser_is_finished(parser) ) {
    return 1;
  } else {
    return 0;
  }
}

int http_parser_has_error(http_parser *parser) {
  return parser->cs == http_parser_error;
}

int http_parser_is_finished(http_parser *parser) {
  return parser->cs >= http_parser_first_final;
}
