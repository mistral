#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "ext/standard/info.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h> 
#include <fcntl.h>
#include <errno.h>
#include <err.h>
#include <stddef.h>

#include <ev.h>

#include "http11_parser.h"
#include "mistral.h"

/*
 * We did use some ideas from: 
 *	https://sie.isc.org/svn/nmsg-httpk/trunk/nmsg-httpk.c
 *	http://github.com/ry/libebb/blob/master/ebb.c
 */

struct client {
	int fd;

	ev_io ev_read;
	ev_io ev_write;
	ev_timer ev_timeout;
	ev_timer ev_goodbye;

	struct ev_loop *loop;
	char *rbuff;
	int rlen;
    int done;

	int close;
};

ev_io ev_accept;
double timeout = 300;

typedef struct _php_event_callback_t { /* {{{ */
	zval *func;
	zval *arg;
} php_event_callback_t;
/* }}} */

php_event_callback_t *callback = NULL;

http_parser parser;

void http_field_cb(void *data, const char *field, size_t flen, const char *value, size_t vlen) {
	zval *server_vars = (zval *)data;
	char *fld = malloc(sizeof(char) * (5 + flen + 1));
	char *val = strndup(value, vlen);
	size_t i;

	memcpy(fld, "HTTP_", 5);
	for (i = 0; i < flen; i++) {
		if (field[i] == '-')
			fld[5+i] = '_';
		else
			fld[5+i] = toupper(field[i]);
	}
	fld[5+flen] = '\0';

	add_assoc_string(server_vars, (char *)fld, (char *)val, 1);
	free(fld);
	free(val);
}

void http_on_element(void *data, int type, const char *at, size_t length) {
	char *line, *val;
	zval *server_vars = (zval *)data;

	switch( type ) {
		case CONTENT_LENGTH:
			line = "CONTENT_LENGTH";
			break;

		case CONTENT_TYPE:
			line = "CONTENT_TYPE";
			break;

		case FRAGMENT:
			line = "FRAGMENT";
			break;

		case HTTP_VERSION:
			line = "HTTP_VERSION";
			break;

		case QUERY_STRING:
			line = "QUERY_STRING";
			break;

		case REQUEST_PATH:
			line = "REQUEST_PATH";
			break;

		case REQUEST_METHOD:
			line = "REQUEST_METHOD";
			break;

		case REQUEST_URI:
			line = "REQUEST_URI";
			break;

		case REQUEST_BODY:
			line = "REQUEST_BODY";
			break;

		default:
			line = "UNKNOWN";
	}

	val = strndup(at, length);
	add_assoc_string(server_vars, (char *) line, (char *) val, 1);
	free(val);
}

void http_on_body(void *data, const char *at, size_t length) {
	char *line, *val;
	zval *server_vars = (zval *)data;

	line = "REQUEST_BODY";

	val = strndup(at, length);
	add_assoc_string(server_vars, (char *) line, (char *) val, 1);
	free(val);
}

static int setnonblock(int fd)
{
	int flags;

	flags = fcntl(fd, F_GETFL);
	if (flags < 0)
		return flags;
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0) 
		return -1;

	return 0;
}

static inline void _php_event_callback_free(php_event_callback_t *this_callback) /* {{{ */
{
	if (!this_callback)
		return;

	zval_ptr_dtor(&this_callback->func);
	if (this_callback->arg)
		zval_ptr_dtor(&this_callback->arg);

	efree(this_callback);
}

static void timeout_cb(struct ev_loop *loop, struct ev_timer *w, int revents) {
	struct client *cli = (struct client *) w->data;
	assert(w == &cli->ev_timeout);

	ev_timer_start(loop, &cli->ev_goodbye);
}

static void goodbye_cb(struct ev_loop *loop, struct ev_timer *w, int revents) {
	struct client *cli = (struct client *) w->data;
	assert(w == &cli->ev_goodbye);

	ev_io_stop(loop, &cli->ev_read);
	ev_io_stop(loop, &cli->ev_write);
	ev_timer_stop(loop, &cli->ev_timeout);

	shutdown(cli->fd, SHUT_WR);

	close(cli->fd);
	free(cli);
}

static void write_cb(struct ev_loop *loop, struct ev_io *w, int revents)
{
	struct client *cli = (struct client *) w->data;

	if (EV_ERROR & revents) {
		ev_timer_start(loop, &cli->ev_goodbye);
		return;
	}

	if (revents & EV_WRITE) {
		if (cli->rbuff) {
			cli->done += write(cli->fd, cli->rbuff + cli->done, cli->rlen - cli->done);
			if (cli->done == cli->rlen) {
				free(cli->rbuff);
				cli->rbuff = NULL;
				cli->rlen = 0;
			} 
		}
		ev_io_stop(EV_A_ w);
	}

	if ((cli->rlen == 0 && cli->close == 1) || timeout == 0.0) {
		ev_timer_start(loop, &cli->ev_goodbye);
	} else {
		ev_timer_again(loop, &cli->ev_timeout);
		ev_io_start(loop, &cli->ev_read);
		if (cli->done < cli->rlen)
			ev_io_start(loop, &cli->ev_write);
	}
}

static inline void* execute_php(void *_cli) {
        int result;
        struct client *cli = (struct client *) _cli;
        zval retval;
        zval *headers[1];
        MAKE_STD_ZVAL(headers[0]);
        array_init(headers[0]);

        cli->done = 0;
        cli->rbuff[cli->rlen] = '\0';

        http_parser_init(&parser);
        parser.http_field = &http_field_cb;
        parser.on_element = &http_on_element;
        parser.header_done = &http_on_element;
        parser.data       = headers[0];
        http_parser_execute(&parser, cli->rbuff, cli->rlen, 0);

        result = call_user_function(EG(function_table), NULL, callback->func, &retval, 1, headers TSRMLS_CC);
        free(cli->rbuff);
        cli->rbuff = NULL;
        cli->rlen  = 0;

        zval_dtor(headers[0]); /* Free's the object contents */
        FREE_ZVAL(headers[0]); /* Free's the object itself */

	if (result == SUCCESS) {
		if (Z_TYPE_P(&retval) == IS_STRING) {
			cli->rlen  = 256 + Z_STRLEN_P(&retval);
			cli->rbuff = (char *) malloc(cli->rlen * sizeof(char));
			cli->rlen  = snprintf(cli->rbuff, (cli->rlen - 1), "HTTP/1.1 200 OK\r\nConnection: %s\r\nContent-Type: text/plain\r\nServer: mistral/0.1\r\nContent-Length: %u\r\n\r\n%s",
					(timeout == 0.0 ? "close" : "keep-alive"), Z_STRLEN_P(&retval), Z_STRVAL_P(&retval));

		} else if (Z_TYPE_P(&retval) == IS_ARRAY) {
			HashTable *arr_hash = Z_ARRVAL_P(&retval);
			HashPosition pointer;
			zval **data;
			int header_len = 11; /* HTTP/1.1 \r\n */
			int body_len = 0;
			int status_len = 0; 
			char *status_code = NULL;
			char *body = NULL;
			char *test;

			for (zend_hash_internal_pointer_reset_ex(arr_hash, &pointer); 
					zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS;
					zend_hash_move_forward_ex(arr_hash, &pointer)) {
				char *key;
				int key_len;
				long index;

				if (zend_hash_get_current_key_ex(arr_hash, &key, &key_len, &index, 0, &pointer) == HASH_KEY_IS_STRING) {
					zval temp = **data;
					zval_copy_ctor(&temp);
					convert_to_string(&temp);

					if (Z_STRLEN(temp) > 0) {
						if (php_strcmp("status_code", key, key_len - 1)) {
							status_code = strndup(Z_STRVAL(temp), Z_STRLEN(temp));
							status_len += Z_STRLEN(temp);
						} else if (php_strcmp("body", key, key_len - 1)) {
							body_len += Z_STRLEN(temp);
						} else { /* So it is a header */
							header_len += key_len - 1;
							header_len += 4; /* : \r\n */
							header_len += Z_STRLEN(temp);

							if (php_strcasecmp("Connection", key, key_len - 1) && php_strcasecmp("close", Z_STRVAL(temp), Z_STRLEN(temp))) {
								cli->close = 1;
							}
						}
					}
					zval_dtor(&temp);
				}
			}

			cli->rlen  = (header_len + (status_len == 0 ? 8 : status_len + 2) + body_len);
			cli->rbuff = (char *) malloc(cli->rlen * sizeof(char));
			test = cli->rbuff;

			if (status_len == 0) {
				memcpy(test, "HTTP/1.1 200 OK\r\n", 17);
				test += 17;
			} else {
				memcpy(test, "HTTP/1.1 ", 9);
				test += 9;
				
				memcpy(test, status_code, status_len);
				test += status_len;
				
				memcpy(test, "\r\n", 2);
				test += 2;
				free(status_code);
			}

			for (zend_hash_internal_pointer_reset_ex(arr_hash, &pointer);
					zend_hash_get_current_data_ex(arr_hash, (void**) &data, &pointer) == SUCCESS;
					zend_hash_move_forward_ex(arr_hash, &pointer)) {
				char *key;
				int key_len;
				long index;

				if (zend_hash_get_current_key_ex(arr_hash, &key, &key_len, &index, 0, &pointer) == HASH_KEY_IS_STRING) {
					zval temp = **data;
					zval_copy_ctor(&temp);
					convert_to_string(&temp);

					if (Z_STRLEN(temp) > 0) {
						if (status_len > 0 && php_strcmp("status_code", key, key_len - 1)) {
							/* do nothing */
						} else if (body_len > 0 && php_strcmp("body", key, key_len - 1)) {
							body = malloc(Z_STRLEN(temp) * sizeof(char));
							memcpy(body, Z_STRVAL(temp), Z_STRLEN(temp));
						} else { /* So it is a header */
							memcpy(test, key, key_len - 1);
							test += (key_len - 1);
							
							memcpy(test, ": ", 2);
							test += 2;
							
							memcpy(test, Z_STRVAL(temp), Z_STRLEN(temp));
							test += Z_STRLEN(temp);
							
							memcpy(test, "\r\n", 2);
							test += 2;
						}
					}
					zval_dtor(&temp);
				}
			}

			memcpy(test, "\r\n", 2);
			test += 2;

			if (body) {
				memcpy(test, body, body_len);
				free(body);
				test += body_len;
			}
		}

		ev_io_start(cli->loop,&cli->ev_write);
	}

	zval_dtor(&retval);

	if (result != SUCCESS) {
		ev_timer_start(cli->loop, &cli->ev_goodbye);
	}
}

static void read_cb(struct ev_loop *loop, struct ev_io *w, int revents)
{
	if ((revents & EV_READ) && callback) {
		struct client *cli = (struct client *) w->data;
		cli->rbuff = malloc(1024);
		if (cli->rbuff) {
			cli->rlen = read(cli->fd,cli->rbuff,1023);
			if (cli->rlen > 0) {
				ev_io_stop(EV_A_ w);
				cli->loop = loop;
				execute_php(cli);
				return;
			} else {
				free(cli->rbuff);
				cli->rbuff = NULL;
				cli->rlen  = 0;
			}
		}
	}

	struct client *cli = (struct client *) w->data;
	ev_io_stop(EV_A_ w);
	ev_timer_start(loop, &cli->ev_goodbye);
}

static void accept_cb(struct ev_loop *loop, struct ev_io *w, int revents)
{
	int client_fd;
	struct client *cli;
	struct sockaddr_in client_addr;
	socklen_t client_len = sizeof(client_addr);
	client_fd = accept(w->fd, (struct sockaddr *)&client_addr, &client_len);
	if (client_fd == -1)
		return;

	cli = calloc(1,sizeof(struct client));
	cli->rbuff = NULL;
	cli->rlen  = 0;
	cli->fd = client_fd;
	if (setnonblock(cli->fd) < 0)
		err(1, "failed to set client socket to non-blocking");

	ev_io_init(&cli->ev_read, read_cb, cli->fd, EV_READ);
	cli->ev_read.data    = cli;

	ev_io_init(&cli->ev_write, write_cb, cli->fd, EV_WRITE);
	cli->ev_write.data   = cli;

	ev_timer_init(&cli->ev_goodbye, goodbye_cb, 0., 0.);
	cli->ev_goodbye.data = cli;

	ev_timer_init(&cli->ev_timeout, timeout_cb, 0., (timeout == 0.0 ? 30.0 : timeout));
	cli->ev_timeout.data = cli;

	ev_io_start(loop,&cli->ev_read);
}

/* {{{ mistral_functions[] */
zend_function_entry mistral_functions[] = {
	PHP_FE(mistral_init, NULL)
		PHP_FE(mistral_register_callback, NULL)
		PHP_FE(mistral_start, NULL)
		{ NULL, NULL, NULL }
};
/* }}} */

/* {{{ mistral_module_entry
 * */
zend_module_entry mistral_module_entry = {
	STANDARD_MODULE_HEADER,
	"mistral",
	mistral_functions,
	NULL,                 /* Replace with NULL if there is nothing to do at php startup   */
	PHP_MSHUTDOWN(mistral),
	NULL,                 /* Replace with NULL if there is nothing to do at request start */
	NULL,                 /* Replace with NULL if there is nothing to do at request end   */
	PHP_MINFO(mistral),
	PHP_MISTRAL_VERSION,
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_MISTRAL
ZEND_GET_MODULE(mistral)
#endif

/* {{{ PHP_MINFO_FUNCTION */
PHP_MINFO_FUNCTION(mistral)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "Mistral support", "enabled");
	php_info_print_table_row(2, "Version",  PHP_MISTRAL_VERSION);
	php_info_print_table_end();
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION */
PHP_MSHUTDOWN_FUNCTION (mistral)
{
	_php_event_callback_free(callback);
}
/* }}} */

PHP_FUNCTION(mistral_init)
{
	struct addrinfo *ai = NULL;
	struct addrinfo hints;
	struct addrinfo *runp;
	int listen_sock = -1;

	char *listen_addr;
	int   listen_addr_len;
	long  listen_port;

	struct sockaddr_storage sin;
	struct sockaddr_in * sa4 = (struct sockaddr_in *) &sin;
	struct sockaddr_in6 * sa6 = (struct sockaddr_in6 *) &sin;

	int reuseaddr_on = 1;
	int keepalive_on = 1;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "sld", &listen_addr, &listen_addr_len, &listen_port, &timeout) == FAILURE) {
		RETURN_NULL();
	}

	if (listen_port < 1 || listen_port > 65535) {
		RETURN_NULL();
	}

	bzero(&hints, sizeof (hints));
	hints.ai_flags = AI_ADDRCONFIG || AI_NUMERICSERV;
	hints.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(listen_addr, "", &hints, &ai) != 0) {
		RETURN_NULL();
	}

	runp = ai;

	while (runp != NULL && listen_sock < 0) {
		listen_sock = socket(runp->ai_family, SOCK_STREAM, 0);
		if (listen_sock < 0) 
			runp = runp->ai_next;
	}

	if (listen_sock < 0) {
		err(1, "listen failed");
	}

	if (timeout <= 0.0) {
		timeout = 0.0;
	} else {
		if (setsockopt(listen_sock, SOL_SOCKET, SO_KEEPALIVE, &keepalive_on, sizeof(keepalive_on)) == -1)
			err(1, "setsockopt failed");
	}

	if (setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_on, sizeof(reuseaddr_on)) == -1)
		err(1, "setsockopt failed");

	if (runp->ai_family == AF_INET6) {
		int null = 0;
		setsockopt(listen_sock, IPPROTO_IPV6, IPV6_V6ONLY, &null, sizeof(null));
	}

	memset(&sin, 0, sizeof(sin));

	sin.ss_family = runp->ai_family;
	switch (sin.ss_family) {
		case AF_INET6:
			inet_pton(sin.ss_family, listen_addr, &(sa6->sin6_addr));
			sa6->sin6_port = htons(listen_port);
			break;
		case AF_INET:
			if ( strchr(listen_addr, ':') )
				/* Invalid IPv4-string. Use the wildcard address. */
				listen_addr = strndup("0.0.0.0", 7);

			inet_pton(sin.ss_family, listen_addr, &(sa4->sin_addr));
			sa4->sin_port = htons(listen_port);
	}

	if (ai)
		freeaddrinfo(ai);

	if (bind(listen_sock, (struct sockaddr *) &sin, sizeof(sin)) < 0)
		err(1, "bind failed");

	if (listen(listen_sock, 5) < 0)
		err(1, "listen failed");

	if (setnonblock(listen_sock) < 0)
		err(1, "failed to set server socket to non-blocking");

	ev_io_init(&ev_accept, accept_cb, listen_sock, EV_READ);
}

/* {{{ proto bool event_set(resource event, resource fd, int events, mixed callback[, mixed arg]) 
 * Taken from pecl::libevent event_set
 */
PHP_FUNCTION(mistral_register_callback)
{
	zval *zcallback, *zarg = NULL;
	php_event_callback_t *new_callback, *old_callback;
	char *func_name;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &zcallback) != SUCCESS) {
		return;
	}

	if (!zend_is_callable(zcallback, 0, &func_name TSRMLS_CC)) {
		php_error_docref(NULL TSRMLS_CC, E_WARNING, "'%s' is not a valid callback", func_name);
		efree(func_name);
		RETURN_FALSE;
	}
	efree(func_name);

	zval_add_ref(&zcallback);

	new_callback = emalloc(sizeof(php_event_callback_t));
	new_callback->func = zcallback;
	new_callback->arg = zarg;

	old_callback = callback;
	callback = new_callback;

	if (old_callback) {
		_php_event_callback_free(old_callback);
	}

	RETURN_TRUE;
}
/* }}} */

PHP_FUNCTION(mistral_start)
{
	struct ev_loop *loop = ev_default_loop(0);

	ev_io_start(loop, &ev_accept);
	ev_loop(loop, 0);
}
